+++
title = "Comunidade"
+++

Essa página explica como a comunidade do Redox OS é organizada e como você pode acessar ela.

Nós seguidos o [Código de Conduta da Rust](https://www.rust-lang.org/policies/code-of-conduct) como regra em todos os nossos canais de comunidade/chats.

## [Chat](https://matrix.to/#/#redox-join:matrix.org)

Matrix é a forma de comunicação oficial com o time/comunidade do Redox OS (aceitamos apenas o Inglês nestas salas, pois não entendemos outras linguagens).

- #redox-join:matrix.org

A rede Matrix tem diferentes clientes, [Element](https://element.io/) é o mais utilizado.

- Entre na sala e não esqueça de solicitar um convite para o espaço do Redox.

(Nós recomendamos que você saia da sala "Join Requests" depois de entrar no espaço do Redox)

## [GitLab](https://gitlab.redox-os.org/redox-os/redox)

Uma forma mais formal de comunicação com os desenvolvedores do Redox, porém mais lento e menos conveniente de conversar.

Envie uma Issue se você tiver problemas compilando/testando ou apenas queira discutir algum assunto, seja funções, estilo de código, inconsistências de código, pequenas mudanças ou correções.

Se você quiser criar uma conta, leia essa [página](https://doc.redox-os.org/book/ch12-01-signing-in-to-gitlab.html).

Se você tiver MRs (merge requests) prontos você precisa enviar o link deles na sala [MRs](https://matrix.to/#/#redox-mrs:matrix.org), antes de entrar nesta sala você precisa solicitar um convite para o espaço Matrix na sala [Join Requests](https://matrix.to/#/#redox-join:matrix.org).

Dessa forma seu MR não será esquecido, evitando conflitos.

## [Lemmy](https://lemmy.world/c/redox)

Nossa alternativa ao Reddit, postamos notícias e tópicos da comunidade.

## [Reddit](https://www.reddit.com/r/Redox/)

Caso queira ver as novidades e discutir sobre.

[reddit.com/r/rust](https://www.reddit.com/r/rust) - Para notícias relacionadas a Rust e discussões.

## [Fosstodon](https://fosstodon.org/@redox)

Nossa alternativa ao Twitter, postamos notícias e tópicos da comunidade.

## [Twitter](https://twitter.com/redox_os)

Notícias e tópicos da comunidade.

## [YouTube](https://www.youtube.com/@RedoxOS)

Demonstrações e reuniões da mesa diretora.

## [Fórum](https://discourse.redox-os.org/)

Nosso fórum histórico com perguntas clássicas/antigas, está inátivo e deve ser usado para consulta histórica.

Se você tem uma pergunta, faça no chat Matrix.

## [Palestras](/talks/)

Palestras sobre o Redox em diversos eventos e conferências.

### Nota

A divulgação da comunidade é uma parte importante no sucesso do Redox, quanto mais pessoas souberem sobre o Redox, mais contribuições podem vir e todos podem se beneficiar.

Você pode fazer a diferença escrevendo artigos, conversando com entusiastas de sistemas operacionais ou procurando por comunidades que podem estar interessadas em conhecer o Redox.
