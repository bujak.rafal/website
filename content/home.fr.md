+++
title = "Accueil"
url = "home"
+++
<div class="row install-row">
  <div class="col-md-8">
    <p class="pitch">
      <b>Redox</b> est un système d'exploitation dans
      le style Unix, écrit en <a style="color: inherit;"
      href="https://www.rust-lang.org/fr/"><b>Rust</b></a>, visant
      à exploiter les innovations de ce langage dans un micro-noyau
      moderne et une suite logicielle complète.
    </p>
  </div>
  <div class="col-md-4 install-box">
    <br/>
    <a class="btn btn-primary" href="/fr/quickstart/">Débuter</a>
    <a class="btn btn-default" href="https://gitlab.redox-os.org/redox-os/redox/">GitLab</a>
  </div>
</div>
<div class="row features">
  <div class="col-md-6">
    <ul class="laundry-list" style="margin-bottom: 0px;">
      <li>Inspiré de Plan 9, Minix, seL4, BSD et Linux</li>
      <li>Écrit en Rust</li>
      <li>Architecture en micro-noyau</li>
      <li>Interface graphique optionnelle incluse - Orbital</li>
      <li>Compatible avec la bibliothèque standard de Rust</li>
    </ul>
  </div>
  <div class="col-md-6">
    <ul class="laundry-list">
      <li>Licence MIT</li>
      <li>Les pilotes s'exécutent en mode utilisateur</li>
      <li>Commandes courantes d'Unix disponibles</li>
      <li>Relibc, une implémentation de Libc écrit en Rust</li>
      <li>Voir <a href="/fr/screens/">Redox en fonctionnement</a></li>
    </ul>
  </div>
</div>
<div class="row features">
  <div class="col-sm-12">
    <div style="font-size: 16px; text-align: center;">
      Redox avec Orbital
    </div>
    <a href="/img/redox-orbital/large.png">
      <picture>
        <source media="(min-width: 1300px)" srcset="/img/redox-orbital/large.webp" type="image/webp">
        <source media="(min-width: 640px)" srcset="/img/redox-orbital/medium.webp" type="image/webp">
        <source media="(min-width: 320px)" srcset="/img/redox-orbital/medium.webp" type="image/webp">
        <source media="(min-width: 1300px)" srcset="/img/redox-orbital/large.png" type="image/png">
        <source media="(min-width: 640px)" srcset="/img/redox-orbital/medium.png" type="image/png">
        <source media="(min-width: 320px)" srcset="/img/redox-orbital/small.png" type="image/png">
        <img src="/img/redox-orbital/medium.png" class="img-responsive" alt="Redox and Orbital">
      </picture>
    </a>
  </div>
</div>
