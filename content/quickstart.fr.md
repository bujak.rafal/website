+++
title = "Débuter"
+++

- [Dernière version](https://www.redox-os.org/news/release-0.8.0/)
- [Commencer (livre de Redox)](https://doc.redox-os.org/book/ch02-00-getting-started.html)
- [Images de redox](https://static.redox-os.org/img/)
- [Versions](https://gitlab.redox-os.org/redox-os/redox/-/releases)

## Compiler Redox

- Compiler Redox à partir du code source est supporté sur Linux et peut être réalisé sur d'autres plateformes, mais ce n'est actuellement pas supporté sur Windows. Pour compiler Redox, veuillez suivre les instructions du [livre de Redox](https://doc.redox-os.org/book/ch02-06-podman-build.html). Si vous êtes sur Pop!_OS, Ubuntu ou Debian, vous pouvez suivre [ces instructions](https://doc.redox-os.org/book/ch02-05-building-redox.html).
